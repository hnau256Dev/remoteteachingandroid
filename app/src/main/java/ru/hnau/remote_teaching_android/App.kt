package ru.hnau.remote_teaching_android

import android.app.Application
import ru.hnau.androidutils.utils.ContextConnector
import ru.hnau.remote_teaching_android.utils.managers.fcm.FCMManager


class App : Application() {

    override fun onCreate() {
        super.onCreate()
        ContextConnector.init(this)
        FCMManager.sendPushTokenIfNeed()
    }

}