package ru.hnau.remote_teaching_android

import android.content.Context
import android.os.Bundle
import android.view.View
import ru.hnau.androidutils.ui.TransparentStatusBarActivity
import ru.hnau.androidutils.ui.view.layer.manager.LayerManager
import ru.hnau.androidutils.ui.view.layer.manager.LayerManagerConnector
import ru.hnau.remote_teaching_android.data.AuthManager
import ru.hnau.remote_teaching_android.layers.LoginLayer
import ru.hnau.remote_teaching_android.layers.main.MainLayer
import ru.hnau.remote_teaching_android.utils.managers.AppActivityConnector

class AppActivity : TransparentStatusBarActivity() {

    private val context: Context
        get() = this

    private val layerManager: LayerManager by lazy {
        val layerManager = LayerManager(context)
        layerManager.showLayer(getInitialLayer())
        return@lazy layerManager
    }

    val layerManagerConnector: LayerManagerConnector
        get() = layerManager

    val contentView: View
        get() = layerManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(layerManager)
        AppActivityConnector.onAppActivityCreated(this)
    }

    override fun onDestroy() {
        AppActivityConnector.onAppActivityDestroyed(this)
        super.onDestroy()
    }

    override fun getIsStatusBarIconsLight() = true

    private fun getInitialLayer() =
        if (AuthManager.logged) MainLayer(context) else LoginLayer(context)

    override fun onBackPressed() {
        if (layerManager.handleGoBack()) {
            return
        }
        super.onBackPressed()
    }

}
