package ru.hnau.remote_teaching_android.api

import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import ru.hnau.remote_teaching_android.api.response_handler.DeferredCallAdapterFactory
import ru.hnau.remote_teaching_android.data.AuthManager
import ru.hnau.remote_teaching_common.api.ApiUtils


val API: RTService = run {

    val httpClient = OkHttpClient.Builder().apply {
        addInterceptor { chain ->
            chain.request().let { request ->
                chain.proceed(
                    request.newBuilder().apply {
                        header(ApiUtils.HEADER_CLIENT_TYPE, ApiConstants.CLIENT_TYPE_NAME)
                        header(ApiUtils.HEADER_CLIENT_VERSION, ApiConstants.CLIENT_VERSION.toString())
                        header(ApiUtils.HEADER_AUTH_TOKEN, AuthManager.token ?: "")
                        method(request.method(), request.body())
                    }.build()
                )
            }
        }
    }.build()

    val retrofit = Retrofit.Builder()
        .baseUrl(ApiConstants.HOST)
        .addConverterFactory(GsonConverterFactory.create())
        .client(httpClient)
        .addCallAdapterFactory(DeferredCallAdapterFactory)
        .build()

    return@run retrofit.create(RTService::class.java)

}