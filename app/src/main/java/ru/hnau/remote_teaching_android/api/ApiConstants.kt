package ru.hnau.remote_teaching_android.api


object ApiConstants {

    const val HOST = "http://hnau.ru:8080"
    const val CLIENT_TYPE_NAME = "ANDROID"
    const val CLIENT_VERSION = 1

}