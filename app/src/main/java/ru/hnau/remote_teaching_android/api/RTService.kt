package ru.hnau.remote_teaching_android.api

import kotlinx.coroutines.Deferred
import retrofit2.http.*
import ru.hnau.jutils.possible.Possible
import ru.hnau.remote_teaching_common.data.StudentsGroup
import ru.hnau.remote_teaching_common.data.User


interface RTService {

    @POST("/client-app-instance/{app-instance-uuid}/push-token/{push-token}")
    fun updatePushToken(
        @Path("push-token") pushToken: String,
        @Path("app-instance-uuid") appInstanceUUID: String
    ): Deferred<Unit>

    @POST("/users/{login}/login")
    fun login(
        @Path("login") login: String,
        @Query("password") password: String,
        @Query("app-instance-uuid") appInstanceUUID: String
    ): Deferred<String>

    @POST("/users/logout")
    fun logout(
        @Query("from-all-clients") fromAllClients: Boolean,
        @Query("app-instance-uuid") appInstanceUUID: String
    ): Deferred<Unit>

    @GET("/users/me")
    fun me(
    ): Deferred<User>

    @POST("/users/me/change-password")
    fun changePassword(
        @Query("new-password") newPassword: String
    ): Deferred<Unit>

    @POST("/users/me/change-fio")
    fun changeFIO(
        @Query("new-name") newName: String,
        @Query("new-surname") newSurname: String,
        @Query("new-patronymic") newPatronymic: String
    ): Deferred<Unit>

    @GET("/teachers")
    fun getAllTeachers(
    ): Deferred<List<User>>

    @PUT("/teachers")
    fun generateNewCreateTeacherActionCode(
    ): Deferred<String>

    @PUT("/teachers/{teacher-login}")
    fun registerTeacher(
        @Path("teacher-login") login: String,
        @Query("password") password: String,
        @Query("action-code") actionCode: String
    ): Deferred<Unit>

    @DELETE("/teachers/{teacher-login}")
    fun deleteTeacher(
        @Path("teacher-login") login: String
    ): Deferred<Unit>

    @POST("/teachers/{teacher-login}/restore-password-code")
    fun generateRestoreTeacherPasswordActionCode(
        @Path("teacher-login") login: String
    ): Deferred<String>

    @POST("/teachers/me/restore-password")
    fun restoreTeacherPassword(
        @Query("new-password") newPassword: String,
        @Query("action-code") actionCode: String
    ): Deferred<String>

    @PUT("/students/{student-login}")
    fun registerStudent(
        @Path("student-login") login: String,
        @Query("password") password: String,
        @Query("action-code") actionCode: String
    ): Deferred<Unit>

    @DELETE("/students/{student-login}")
    fun deleteStudent(
        @Path("student-login") login: String
    ): Deferred<Unit>

    @POST("/students/{student-login}/restore-password-code")
    fun getRestoreStudentPasswordActionCode(
        @Path("student-login") login: String
    ): Deferred<String>

    @POST("/students/me/restore-password")
    fun restoreStudentPassword(
        @Query("new-password") newPassword: String,
        @Query("action-code") actionCode: String
    ): Deferred<String>

    @GET("/students-groups")
    fun getAllStudentsGroups(
    ): Deferred<List<StudentsGroup>>

    @GET("/students-groups/{students-group-name}/students")
    fun getStudentsOfGroup(
        @Path("students-group-name") studentsGroupName: String
    ): Deferred<List<User>>

    @PUT("/students-groups/{students-group-name}")
    fun createStudentsGroup(
        @Path("students-group-name") studentsGroupName: String
    ): Deferred<Unit>

    @POST("/students-groups/{students-group-name}/archive")
    fun archiveStudentsGroup(
        @Path("students-group-name") studentsGroupName: String
    ): Deferred<Unit>

    @POST("/students-groups/{students-group-name}/unarchive")
    fun unarchiveStudentsGroup(
        @Path("students-group-name") studentsGroupName: String
    ): Deferred<Unit>

    @DELETE("/students-groups/{students-group-name}")
    fun deleteStudentsGroup(
        @Path("students-group-name") studentsGroupName: String
    ): Deferred<Unit>

    @PUT("/students")
    fun generateStudentsGroupRegistrationCode(
        @Query("students-group-name") studentsGroupName: String
    ): Deferred<String>


}