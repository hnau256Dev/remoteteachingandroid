package ru.hnau.remote_teaching_android.api.response_handler

import kotlinx.coroutines.Deferred
import retrofit2.Call
import retrofit2.CallAdapter
import retrofit2.Callback
import retrofit2.Response
import ru.hnau.jutils.coroutines.deferred.deferred
import ru.hnau.remote_teaching_common.api.ApiResponse
import ru.hnau.remote_teaching_common.exception.ApiException
import java.lang.reflect.Type


class DeferredCallAdapter<T : Any>(
    private val responseType: Type
) : CallAdapter<ApiResponse<T>, Deferred<T>> {

    override fun responseType() = responseType

    override fun adapt(call: Call<ApiResponse<T>>) = deferred<T> {

        call.enqueue(object : Callback<ApiResponse<T>> {

            override fun onFailure(call: Call<ApiResponse<T>>, t: Throwable) {
                completeExceptionally(ApiException.NETWORK)
            }

            override fun onResponse(call: Call<ApiResponse<T>>, response: Response<ApiResponse<T>>) {
                if (!response.isSuccessful) {
                    completeExceptionally(ApiException.NETWORK)
                    return
                }
                response.body()!!.handle(
                    onSuccess = this@deferred::complete,
                    onError = this@deferred::completeExceptionally
                )
            }
        })

        invokeOnCompletion {
            if (isCancelled) {
                call.cancel()
            }
        }

    }

}