package ru.hnau.remote_teaching_android.data

import ru.hnau.jutils.TimeValue


object DataUtils {

    val DEFAULT_DATA_LIFETIME = TimeValue.MINUTE * 10

}