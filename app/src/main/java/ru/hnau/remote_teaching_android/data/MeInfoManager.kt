package ru.hnau.remote_teaching_android.data


import ru.hnau.remote_teaching_android.api.API
import ru.hnau.remote_teaching_common.data.User


object MeInfoManager : RTDataManager<User>(
        invalidateAfterUserLogin = true
) {

    override suspend fun getNewValue() =
        API.me().await()

}