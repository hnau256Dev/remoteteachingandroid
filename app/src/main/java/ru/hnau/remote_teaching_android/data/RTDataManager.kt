package ru.hnau.remote_teaching_android.data

import ru.hnau.jutils.TimeValue
import ru.hnau.jutils.coroutines.SuspendProducer


abstract class RTDataManager<T>(
    dataLifetime: TimeValue = DataUtils.DEFAULT_DATA_LIFETIME,
    invalidateAfterUserLogin: Boolean = false
) : SuspendProducer<T>(
    dataLifetime = dataLifetime
) {

    init {
        if (invalidateAfterUserLogin) {
            AuthManager.onUserLoggedProducer.attach { clear() }
        }
    }

    protected fun updateOrInvalidate(updater: (lastValue: T) -> T) {
        val existence = existence
        if (existence == null) {
            clear()
            return
        }
        val newValue = updater.invoke(existence.value)
        setValue(newValue)
    }

}