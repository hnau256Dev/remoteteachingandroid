package ru.hnau.remote_teaching_android.data

import ru.hnau.jutils.cache.AutoCache
import ru.hnau.remote_teaching_android.api.API
import ru.hnau.remote_teaching_common.data.User


class StudentsOfGroupManager(
        private val studentsGroupName: String
) : RTDataManager<List<User>>() {

    companion object : AutoCache<String, StudentsOfGroupManager>(
            getter = ::StudentsOfGroupManager,
            capacity = 16
    )

    override suspend fun getNewValue() =
            API.getStudentsOfGroup(studentsGroupName).await()

    suspend fun remove(login: String) {
        API.deleteStudent(login).await()
        updateOrInvalidate { oldValue ->
            oldValue.filter { it.login != login }
        }
    }

    suspend fun generateRestorePasswordActionCode(login: String) =
            API.getRestoreStudentPasswordActionCode(login).await()



}