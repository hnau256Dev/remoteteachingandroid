package ru.hnau.remote_teaching_android.data

import ru.hnau.remote_teaching_android.api.API
import ru.hnau.remote_teaching_android.utils.extensions.sortKey
import ru.hnau.remote_teaching_common.data.User


object TeachersListManager : RTDataManager<List<User>>() {

    override suspend fun getNewValue() =
        API.getAllTeachers().await().sortedBy { it.sortKey }

    suspend fun remove(login: String) {
        API.deleteTeacher(login).await()
        updateOrInvalidate { oldValue ->
            oldValue.filter { it.login != login }
        }
    }

    suspend fun generateCreateActionCode() =
        API.generateNewCreateTeacherActionCode().await()

    suspend fun generateRestorePasswordActionCode(login: String) =
        API.generateRestoreTeacherPasswordActionCode(login).await()

}