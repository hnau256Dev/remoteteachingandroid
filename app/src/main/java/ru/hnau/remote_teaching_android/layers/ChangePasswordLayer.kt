package ru.hnau.remote_teaching_android.layers

import android.content.Context
import android.text.InputType
import android.text.method.PasswordTransformationMethod
import ru.hnau.androidutils.context_getters.StringGetter
import ru.hnau.androidutils.ui.view.addLinearSeparator
import ru.hnau.androidutils.ui.view.utils.*
import ru.hnau.androidutils.utils.shortToast
import ru.hnau.remote_teaching_android.R
import ru.hnau.remote_teaching_android.api.API
import ru.hnau.remote_teaching_android.layers.base.AppLayer
import ru.hnau.remote_teaching_android.ui.hierarchy_utils.addBottomButtonView
import ru.hnau.remote_teaching_android.ui.hierarchy_utils.addFgSmallInputLabelView
import ru.hnau.remote_teaching_android.ui.input.InputViewInfo
import ru.hnau.remote_teaching_android.ui.input.addInput
import ru.hnau.remote_teaching_android.utils.managers.SizeManager
import ru.hnau.remote_teaching_common.utils.Validators
import ru.hnau.remote_teaching_common.exception.ApiException


class ChangePasswordLayer(
    context: Context
) : AppLayer(
    context = context
) {

    override val title = StringGetter(R.string.change_password_layer_title)

    override fun afterCreate() {
        super.afterCreate()

        setPadding(SizeManager.LARGE_SEPARATION, SizeManager.DEFAULT_SEPARATION)

        addFgSmallInputLabelView(StringGetter(R.string.change_password_layer_new_password))

        val passwordInput = addInput(
            info = InputViewInfo(
                maxLength = Validators.MAX_PASSWORD_LENGTH,
                transformationMethod = PasswordTransformationMethod.getInstance(),
                inputType = InputType.TYPE_TEXT_VARIATION_PASSWORD
            )
        )


        addFgSmallInputLabelView(StringGetter(R.string.change_password_layer_new_password_repeat))

        val passwordRepeatInput = addInput(
            info = InputViewInfo(
                maxLength = Validators.MAX_PASSWORD_LENGTH,
                transformationMethod = PasswordTransformationMethod.getInstance(),
                inputType = InputType.TYPE_TEXT_VARIATION_PASSWORD
            )
        )

        addLinearSeparator()

        addBottomButtonView(
            text = StringGetter(R.string.change_password_layer_change_password),
            onClick = {
                changePassword(
                    passwordInput.text.toString(),
                    passwordRepeatInput.text.toString()
                )
            }
        )

    }

    private fun changePassword(newPassword: String, newPasswordRepeat: String) {
        tryExecute {
            suspendLockedProducer.executeLocked {

                Validators.validateUserPasswordOrThrow(newPassword)
                if (newPassword != newPasswordRepeat) {
                    throw ApiException.raw(context.getString(R.string.change_password_layer_passwords_are_different))
                }

                API.changePassword(newPassword).await()
                shortToast(StringGetter(R.string.change_password_layer_success))
                managerConnector.goBack()
            }
        }
    }

}