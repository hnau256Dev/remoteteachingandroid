package ru.hnau.remote_teaching_android.layers

import android.content.Context
import ru.hnau.androidutils.context_getters.StringGetter
import ru.hnau.androidutils.context_getters.toGetter
import ru.hnau.androidutils.ui.view.addLinearSeparator
import ru.hnau.androidutils.ui.view.utils.setPadding
import ru.hnau.androidutils.utils.shortToast
import ru.hnau.remote_teaching_android.R
import ru.hnau.remote_teaching_android.api.API
import ru.hnau.remote_teaching_android.data.MeInfoManager
import ru.hnau.remote_teaching_android.layers.base.AppLayer
import ru.hnau.remote_teaching_android.ui.hierarchy_utils.addBottomButtonView
import ru.hnau.remote_teaching_android.ui.hierarchy_utils.addFgSmallInputLabelView
import ru.hnau.remote_teaching_android.ui.hierarchy_utils.addSmallPrimaryTextAndBorderButtonView
import ru.hnau.remote_teaching_android.ui.input.InputViewInfo
import ru.hnau.remote_teaching_android.ui.input.addInput
import ru.hnau.remote_teaching_android.utils.managers.SizeManager
import ru.hnau.remote_teaching_common.data.User
import ru.hnau.remote_teaching_common.utils.Validators


class ProfileLayer(
    context: Context
) : AppLayer(
    context = context
) {

    override val title = StringGetter(R.string.profile_layer_title)

    override fun afterCreate() {
        super.afterCreate()

        val user = MeInfoManager.existence?.value!!

        setPadding(SizeManager.LARGE_SEPARATION, SizeManager.DEFAULT_SEPARATION)

        addFgSmallInputLabelView(StringGetter(R.string.profile_layer_surname))

        val surnameInput = addInput(
            info = InputViewInfo(
                maxLength = Validators.MAX_SURNAME_LENGTH
            ),
            text = user.surname.toGetter()
        )

        addFgSmallInputLabelView(StringGetter(R.string.profile_layer_name))

        val nameInput = addInput(
            info = InputViewInfo(
                maxLength = Validators.MAX_NAME_LENGTH
            ),
            text = user.name.toGetter()
        )

        addFgSmallInputLabelView(StringGetter(R.string.profile_layer_patronymic))

        val patronymicInput = addInput(
            info = InputViewInfo(
                maxLength = Validators.MAX_PATRONYMIC_LENGTH
            ),
            text = user.patronymic.toGetter()
        )

        addLinearSeparator()

        addSmallPrimaryTextAndBorderButtonView(
            text = StringGetter(R.string.profile_layer_change_password),
            onClick = { showLayer(ChangePasswordLayer(context)) }
        )

        addLinearSeparator()

        addBottomButtonView(
            text = StringGetter(R.string.dialog_save),
            onClick = {
                changeFIO(
                    user,
                    nameInput.text.toString(),
                    surnameInput.text.toString(),
                    patronymicInput.text.toString()
                )
            }
        )

    }

    private fun changeFIO(
        user: User,
        newName: String,
        newSurname: String,
        newPatronymic: String
    ) {
        tryExecute {
            suspendLockedProducer.executeLocked {

                Validators.validateUserNameOrThrow(newName)
                Validators.validateUserSurnameOrThrow(newSurname)
                Validators.validateUserPatronymicOrThrow(newPatronymic)

                if (
                    user.name != newName ||
                    user.surname != newSurname ||
                    user.patronymic != newPatronymic
                ) {
                    API.changeFIO(newName, newSurname, newPatronymic).await()
                    MeInfoManager.setValue(
                        user.copy(
                            name = newName,
                            surname = newSurname,
                            patronymic = newPatronymic
                        )
                    )
                }

                shortToast(StringGetter(R.string.profile_layer_success))
                managerConnector.goBack()
            }
        }
    }


}