package ru.hnau.remote_teaching_android.layers

import android.content.Context
import ru.hnau.androidutils.context_getters.StringGetter
import ru.hnau.remote_teaching_android.R
import ru.hnau.remote_teaching_android.layers.base.AppLayer


class SettingsLayer(
    context: Context
) : AppLayer(
    context = context
) {

    override val title = StringGetter(R.string.settings_layer_title)



}