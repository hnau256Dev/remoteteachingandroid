package ru.hnau.remote_teaching_android.layers.base

import android.annotation.SuppressLint
import android.content.Context
import android.view.View
import android.widget.FrameLayout
import android.widget.LinearLayout
import ru.hnau.androidutils.context_getters.DrawableGetter
import ru.hnau.androidutils.context_getters.StringGetter
import ru.hnau.androidutils.context_getters.toGetter
import ru.hnau.androidutils.coroutines.createUIJob
import ru.hnau.androidutils.ui.drawables.layout_drawable.LayoutDrawable
import ru.hnau.androidutils.ui.drawables.layout_drawable.createIndependent
import ru.hnau.androidutils.ui.utils.h_gravity.HGravity
import ru.hnau.androidutils.ui.view.header.Header
import ru.hnau.androidutils.ui.view.header.HeaderTitle
import ru.hnau.androidutils.ui.view.header.back.button.addHeaderBackButton
import ru.hnau.androidutils.ui.view.header.button.HeaderIconButton
import ru.hnau.androidutils.ui.view.layer.layer.Layer
import ru.hnau.androidutils.ui.view.layer.layer.ManagerConnector
import ru.hnau.androidutils.ui.view.layer.manager.LayerManagerConnector
import ru.hnau.androidutils.ui.view.utils.*
import ru.hnau.jutils.producer.locked_producer.SuspendLockedProducer
import ru.hnau.remote_teaching_android.R
import ru.hnau.remote_teaching_android.ui.hierarchy_utils.addWaiter
import ru.hnau.remote_teaching_android.utils.*
import ru.hnau.remote_teaching_android.utils.managers.ColorManager
import ru.hnau.remote_teaching_android.utils.managers.FontManager
import ru.hnau.remote_teaching_android.utils.managers.SizeManager
import kotlin.coroutines.CoroutineContext


@SuppressLint("ViewConstructor")
@Suppress("LeakingThis")
abstract class AppLayer(
        context: Context,
        private val showGoBackButton: Boolean = true
) : LinearLayout(
        context
), Layer, CoroutinesExecutor {

    override val view = this

    protected abstract val title: StringGetter

    private val uiJob = createUIJob()

    protected val suspendLockedProducer = SuspendLockedProducer()

    @ManagerConnector
    protected lateinit var managerConnector: LayerManagerConnector

    private val headerOptionsMenuButton = HeaderIconButton(
            context = context,
            icon = LayoutDrawable.createIndependent(context, DrawableGetter(R.drawable.ic_options_white)).toGetter(),
            rippleDrawInfo = ColorManager.BG_ON_PRIMARY_RIPPLE_INFO,
            onClick = this::showOptionsMenu
    )

    private val headerTitle: HeaderTitle by lazy {
        HeaderTitle(
                context = context,
                textColor = ColorManager.BG,
                fontType = FontManager.DEFAULT,
                textSize = SizeManager.TEXT_20,
                gravity = HGravity.CENTER
        ).apply {
            if (!showGoBackButton) {
                setLeftPadding(HeaderTitle.DEFAULT_HORIZONTAL_PADDING + Header.DEFAULT_HEIGHT)
            }
        }
    }

    private val header: View by lazy {
        Header(
                context = context,
                underStatusBar = true,
                headerBackgroundColor = ColorManager.PRIMARY
        ).apply {

            if (showGoBackButton) {
                addHeaderBackButton(
                        rippleDrawInfo = ColorManager.BG_ON_PRIMARY_RIPPLE_INFO,
                        color = ColorManager.BG,
                        onClick = { managerConnector.goBack() }
                )
            }

            addView(headerTitle)
            addView(headerOptionsMenuButton)

            updateTitle()
        }
    }

    protected fun updateTitle() {
        headerTitle.text = title
    }

    private fun showOptionsMenu(): Unit =
            AppLayerMenu.show(headerOptionsMenuButton, this)

    private val contentContainer = LinearLayout(context).apply {
        orientation = VERTICAL
        setCenterForegroundGravity()
    }

    private val sceneView = FrameLayout(context).apply {

        setBackgroundColor(ColorManager.BG)
        setLinearParams(MATCH_PARENT, 0, 1f)

        addView(contentContainer)
        addWaiter(suspendLockedProducer)
    }

    protected fun tryExecute(action: suspend CoroutineContext.() -> Unit) =
            uiJob.tryExecute(action)

    fun showLayer(layer: AppLayer, clearStack: Boolean = false) {
        managerConnector.showLayer(layer, clearStack)
    }

    override fun executeCoroutine(coroutine: suspend () -> Unit) =
        tryExecute { suspendLockedProducer.executeLocked(coroutine) }

    override fun afterCreate() {
        super.afterCreate()
        orientation = VERTICAL
        super.addView(header)
        super.addView(sceneView)
    }

    override fun addView(child: View) =
            contentContainer.addView(child)

    override fun setPadding(left: Int, top: Int, right: Int, bottom: Int) {
        contentContainer.setPadding(left, top, right, bottom)
    }

}