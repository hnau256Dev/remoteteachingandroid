package ru.hnau.remote_teaching_android.layers.base

import android.view.View
import ru.hnau.androidutils.context_getters.StringGetter
import ru.hnau.remote_teaching_android.R
import ru.hnau.remote_teaching_android.data.AuthManager
import ru.hnau.remote_teaching_android.layers.ProfileLayer
import ru.hnau.remote_teaching_android.layers.SettingsLayer
import ru.hnau.remote_teaching_android.utils.managers.AppActivityConnector
import ru.hnau.remote_teaching_android.utils.CoroutinesExecutor
import ru.hnau.remote_teaching_android.utils.managers.OptionsMenuManager


object AppLayerMenu {

    fun show(
        anchor: View,
        coroutinesExecutor: CoroutinesExecutor
    ) =
        OptionsMenuManager.show(anchor, getItems(coroutinesExecutor))

    private fun getItems(
        coroutinesExecutor: CoroutinesExecutor
    ) =
        getAnonymousItems() +
                (if (AuthManager.logged) getProfileItems(coroutinesExecutor) else emptyList())

    private fun getAnonymousItems() = listOf(
        OptionsMenuManager.Item(
            title = StringGetter(R.string.options_menu_item_settings),
            onClick = { AppActivityConnector.showLayer(::SettingsLayer) }
        )
    )

    private fun getProfileItems(
        coroutinesExecutor: CoroutinesExecutor
    ) = listOf(
        OptionsMenuManager.Item(
            title = StringGetter(R.string.options_menu_item_profile),
            onClick = { AppActivityConnector.showLayer(::ProfileLayer) }
        ),
        OptionsMenuManager.Item(
            title = StringGetter(R.string.options_menu_item_logout),
            onClick = { askAndLogout(coroutinesExecutor) }
        )
    )

    private fun askAndLogout(
        coroutinesExecutor: CoroutinesExecutor
    ) =
        AppActivityConnector.showConfirmDialog(
            title = StringGetter(R.string.options_menu_ask_logout_title),
            text = StringGetter(R.string.options_menu_ask_logout_text),
            confirmText = StringGetter(R.string.options_menu_confirm_logout),
            onConfirm = { AuthManager.logout(coroutinesExecutor) }
        )

}