package ru.hnau.remote_teaching_android.layers.main

import android.content.Context
import android.view.ViewGroup
import ru.hnau.androidutils.context_getters.StringGetter
import ru.hnau.androidutils.context_getters.toGetter
import ru.hnau.androidutils.ui.view.utils.MATCH_PARENT
import ru.hnau.androidutils.ui.view.utils.setLinearParams
import ru.hnau.remote_teaching_android.R
import ru.hnau.remote_teaching_android.data.MeInfoManager
import ru.hnau.remote_teaching_android.layers.base.AppLayer
import ru.hnau.remote_teaching_android.layers.main.admin.AdminContentView
import ru.hnau.remote_teaching_android.layers.main.teacher.TeacherContentView
import ru.hnau.remote_teaching_android.ui.hierarchy_utils.addSuspendLoaderView
import ru.hnau.remote_teaching_common.data.User
import ru.hnau.remote_teaching_common.data.UserRole
import ru.hnau.remote_teaching_common.exception.ApiException


class MainLayer(
        context: Context
) : AppLayer(
        context = context,
        showGoBackButton = false
) {

    override val title: StringGetter
        get() = MeInfoManager.existence?.value?.login
                ?.let { "@$it".toGetter() }
                ?: StringGetter(R.string.app_name)

    init {
        addSuspendLoaderView(
                producer = MeInfoManager,
                contentViewGenerator = this::createContentView
        ) {
            setLinearParams(MATCH_PARENT, 0, 1f)
        }
    }

    private fun createContentView(user: User): ViewGroup {
        updateTitle()
        return when (user.role) {
            UserRole.ADMIN -> AdminContentView(context)
            UserRole.TEACHER -> TeacherContentView(context)
            else -> throw ApiException.UNDEFINED
        }
    }

}