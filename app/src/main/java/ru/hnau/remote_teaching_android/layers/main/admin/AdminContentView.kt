package ru.hnau.remote_teaching_android.layers.main.admin

import android.content.Context
import android.view.View
import android.widget.FrameLayout
import ru.hnau.androidutils.context_getters.DrawableGetter
import ru.hnau.androidutils.context_getters.StringGetter
import ru.hnau.androidutils.coroutines.createUIJob
import ru.hnau.androidutils.ui.view.auto_swipe_refresh_view.addAutoSwipeRefreshView
import ru.hnau.androidutils.utils.shortToast
import ru.hnau.jutils.producer.SimpleDataProducer
import ru.hnau.jutils.producer.locked_producer.SuspendLockedProducer
import ru.hnau.remote_teaching_android.R
import ru.hnau.remote_teaching_android.data.TeachersListManager
import ru.hnau.remote_teaching_android.ui.empty_info.EmptyInfoView
import ru.hnau.remote_teaching_android.ui.hierarchy_utils.addPrimaryFab
import ru.hnau.remote_teaching_android.ui.hierarchy_utils.addSuspendLoaderView
import ru.hnau.remote_teaching_android.ui.hierarchy_utils.addWaiter
import ru.hnau.remote_teaching_android.ui.list.UserList
import ru.hnau.remote_teaching_android.utils.*
import ru.hnau.remote_teaching_android.utils.extensions.fioOrLogin
import ru.hnau.remote_teaching_android.utils.extensions.showInfoDialog
import ru.hnau.remote_teaching_android.utils.managers.AppActivityConnector
import ru.hnau.remote_teaching_android.utils.managers.ColorManager
import ru.hnau.remote_teaching_common.data.ActionCodeType
import ru.hnau.remote_teaching_common.data.User


class AdminContentView(
    context: Context
) : FrameLayout(
    context
) {

    private val suspendLockedProducer = SuspendLockedProducer()

    private val uiJob = createUIJob()

    private var teachersProducer = SimpleDataProducer<List<User>>(emptyList())

    private val listView: UserList by lazy {
        UserList(
            context = context,
            onClick = this::onTeacherClick,
            itemsProducer = teachersProducer
        )
    }

    private val noTeachersInfoView: View by lazy {

        EmptyInfoView(
            context = context,
            text = StringGetter(R.string.admin_main_view_no_teachers_title),
            button = StringGetter(R.string.admin_main_view_no_teachers_button) to this::generateTeacherRegisterActionCode
        )

    }

    init {

        addAutoSwipeRefreshView(
            color = ColorManager.PRIMARY,
            updateContent = TeachersListManager::clear
        ) {

            addSuspendLoaderView(
                producer = TeachersListManager,
                contentViewGenerator = { teachers ->
                    if (teachers.isEmpty()) {
                        noTeachersInfoView
                    } else {
                        teachersProducer.content = teachers
                        listView
                    }
                }
            )

        }

        addPrimaryFab(
            icon = DrawableGetter(R.drawable.ic_add_white),
            onClick = this::generateTeacherRegisterActionCode
        )

        addWaiter(suspendLockedProducer)

    }

    private fun onTeacherClick(teacher: User) {
        AppActivityConnector.showBottomSheet {
            title(StringGetter(R.string.admin_main_view_teacher_options_title, teacher.fioOrLogin))

            closeItem(
                StringGetter(R.string.admin_main_view_teacher_options_restore_password)
            ) { onTeacherRestorePasswordClick(teacher) }

            closeItem(
                StringGetter(R.string.admin_main_view_teacher_options_delete)
            ) { onTeacherRemoveClick(teacher) }
        }
    }

    private fun onTeacherRestorePasswordClick(teacher: User) {
        uiJob.tryExecute {
            val actionCode = suspendLockedProducer.executeLocked {
                TeachersListManager.generateRestorePasswordActionCode(teacher.login)
            }
            ActionCodeType.RESTORE_TEACHER_PASSWORD.showInfoDialog(actionCode)
        }
    }

    private fun onTeacherRemoveClick(teacher: User) {
        AppActivityConnector.showConfirmDialog(
            title = StringGetter(R.string.admin_main_view_remove_teacher_confirm_title),
            text = StringGetter(R.string.admin_main_view_remove_teacher_confirm_text, teacher.fioOrLogin),
            confirmText = StringGetter(R.string.dialog_remove)
        ) {
            uiJob.tryExecute {
                suspendLockedProducer.executeLocked {
                    TeachersListManager.remove(teacher.login)
                    shortToast(StringGetter(R.string.admin_main_view_remove_teacher_success, teacher.fioOrLogin))
                }
            }
        }
    }

    private fun generateTeacherRegisterActionCode() {
        uiJob.tryExecute {
            val actionCode = suspendLockedProducer.executeLocked {
                TeachersListManager.generateCreateActionCode()
            }
            ActionCodeType.CREATE_TEACHER.showInfoDialog(actionCode)
        }
    }


}