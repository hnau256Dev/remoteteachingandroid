package ru.hnau.remote_teaching_android.layers.main.teacher

import android.content.Context
import ru.hnau.androidutils.context_getters.StringGetter
import ru.hnau.remote_teaching_android.R
import ru.hnau.remote_teaching_android.layers.main.teacher.courses.TeacherCoursesPage
import ru.hnau.remote_teaching_android.layers.main.teacher.groups.TeacherGroupsPage
import ru.hnau.remote_teaching_android.ui.pager.Pager
import ru.hnau.remote_teaching_android.ui.pager.PagerPage


class TeacherContentView(
    context: Context
) : Pager(
    context = context,
    pages = listOf(
        PagerPage(
            title = StringGetter(R.string.teacher_main_view_groups_title),
            viewCreator = { TeacherGroupsPage(context) }
        ),
        PagerPage(
            title = StringGetter(R.string.teacher_main_view_courses_title),
            viewCreator = { TeacherCoursesPage(context) }
        )
    )
)