package ru.hnau.remote_teaching_android.layers.main.teacher.groups

import android.content.Context
import android.view.View
import android.widget.FrameLayout
import ru.hnau.androidutils.context_getters.DrawableGetter
import ru.hnau.androidutils.context_getters.StringGetter
import ru.hnau.androidutils.coroutines.createUIJob
import ru.hnau.androidutils.ui.view.auto_swipe_refresh_view.addAutoSwipeRefreshView
import ru.hnau.androidutils.utils.shortToast
import ru.hnau.jutils.producer.SimpleDataProducer
import ru.hnau.jutils.producer.locked_producer.SuspendLockedProducer
import ru.hnau.remote_teaching_android.R
import ru.hnau.remote_teaching_android.data.StudentsGroupsListManager
import ru.hnau.remote_teaching_android.ui.empty_info.EmptyInfoView
import ru.hnau.remote_teaching_android.ui.hierarchy_utils.addPrimaryFab
import ru.hnau.remote_teaching_android.ui.hierarchy_utils.addSuspendLoaderView
import ru.hnau.remote_teaching_android.ui.hierarchy_utils.addWaiter
import ru.hnau.remote_teaching_android.ui.input.InputViewInfo
import ru.hnau.remote_teaching_android.ui.list.StudentsGroupList
import ru.hnau.remote_teaching_android.utils.*
import ru.hnau.remote_teaching_android.utils.managers.AppActivityConnector
import ru.hnau.remote_teaching_android.utils.managers.ColorManager
import ru.hnau.remote_teaching_common.data.StudentsGroup
import ru.hnau.remote_teaching_common.utils.Validators


class TeacherGroupsPage(
    context: Context
) : FrameLayout(
    context
), CoroutinesExecutor {

    private val suspendLockedProducer = SuspendLockedProducer()

    private val uiJob = createUIJob()

    private var studentsGroupsProducer = SimpleDataProducer<List<StudentsGroup>>(emptyList())

    private val listView: StudentsGroupList by lazy {
        StudentsGroupList(
            context = context,
            onClick = this::onStudentsGroupClick,
            itemsProducer = studentsGroupsProducer
        )
    }

    private val noStudentsGroupsInfoView: View by lazy {

        EmptyInfoView(
            context = context,
            text = StringGetter(R.string.teacher_main_view_groups_no_groups_title),
            button = StringGetter(R.string.teacher_main_view_groups_no_groups_button) to this::onAddGroupClicked
        )

    }

    init {

        addAutoSwipeRefreshView(
            color = ColorManager.PRIMARY,
            updateContent = StudentsGroupsListManager::clear
        ) {

            addSuspendLoaderView(
                producer = StudentsGroupsListManager,
                contentViewGenerator = { studentsGroups ->
                    if (studentsGroups.isEmpty()) {
                        noStudentsGroupsInfoView
                    } else {
                        studentsGroupsProducer.content = studentsGroups
                        listView
                    }
                }
            )

        }

        addPrimaryFab(
            icon = DrawableGetter(R.drawable.ic_add_white),
            onClick = this::onAddGroupClicked
        )

        addWaiter(suspendLockedProducer)

    }

    private fun onStudentsGroupClick(studentsGroup: StudentsGroup) {
        AppActivityConnector.showBottomSheet {
            title(StringGetter(R.string.teacher_main_view_groups_options_title, studentsGroup.name))

            TeacherGroupsPageUtils.getStudentsGroupActions(
                studentsGroup = studentsGroup,
                coroutinesExecutor = this@TeacherGroupsPage
            ).forEach { (title, action) ->
                closeItem(title, action)
            }
        }
    }

    override fun executeCoroutine(coroutine: suspend () -> Unit) =
        uiJob.tryExecute { suspendLockedProducer.executeLocked(coroutine) }

    private fun onAddGroupClicked() {
        AppActivityConnector.showInputDialog(
            title = StringGetter(R.string.teacher_main_view_groups_create_new_dialog_title),
            text = StringGetter(R.string.teacher_main_view_groups_create_new_dialog_text),
            confirmButtonText = StringGetter(R.string.teacher_main_view_groups_create_new_dialog_button),
            inputInfo = InputViewInfo(
                maxLength = Validators.MAX_STUDENTS_GROUP_NAME_LENGTH
            ),
            onConfirm = this::onNewGroupNameEntered
        )
    }

    private fun onNewGroupNameEntered(groupName: String): Boolean {
        tryOrHandleError {
            Validators.validateStudentsGroupNameOrThrow(groupName)
        } ?: return false

        uiJob.tryExecute {
            suspendLockedProducer.executeLocked {
                StudentsGroupsListManager.createNew(groupName)
                shortToast(StringGetter(R.string.teacher_main_view_groups_create_new_success, groupName))
            }
        }
        return true
    }

}