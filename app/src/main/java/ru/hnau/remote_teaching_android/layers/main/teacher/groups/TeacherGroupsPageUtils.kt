package ru.hnau.remote_teaching_android.layers.main.teacher.groups

import ru.hnau.androidutils.context_getters.StringGetter
import ru.hnau.androidutils.utils.showToast
import ru.hnau.remote_teaching_android.R
import ru.hnau.remote_teaching_android.data.StudentsGroupsListManager
import ru.hnau.remote_teaching_android.layers.students.StudentsOfGroupLayer
import ru.hnau.remote_teaching_android.utils.managers.AppActivityConnector
import ru.hnau.remote_teaching_android.utils.CoroutinesExecutor
import ru.hnau.remote_teaching_android.utils.extensions.showInfoDialog
import ru.hnau.remote_teaching_common.data.ActionCodeType
import ru.hnau.remote_teaching_common.data.StudentsGroup


object TeacherGroupsPageUtils {

    data class Action(
            val title: StringGetter,
            val action: () -> Unit
    )

    private fun getUnarchivedStudentsGroupActions(
            studentsGroup: StudentsGroup,
            coroutinesExecutor: CoroutinesExecutor
    ) = listOf(

            Action(
                    title = StringGetter(R.string.teacher_main_view_groups_options_students),
                    action = {
                        AppActivityConnector.showLayer({ StudentsOfGroupLayer.newInstance(it, studentsGroup) })
                    }
            ),

            Action(
                    title = StringGetter(R.string.teacher_main_view_groups_options_courses),
                    action = {
                        //TODO go to courses
                    }
            ),
            Action(
                    title = StringGetter(R.string.teacher_main_view_groups_options_archive),
                    action = {
                        AppActivityConnector.showConfirmDialog(
                                title = StringGetter(R.string.teacher_main_view_groups_option_archive_confirm_dialog_title),
                                text = StringGetter(
                                        R.string.teacher_main_view_groups_option_archive_confirm_dialog_text,
                                        studentsGroup.name
                                ),
                                confirmText = StringGetter(R.string.teacher_main_view_groups_option_archive_confirm_dialog_button)
                        ) {
                            coroutinesExecutor.executeCoroutine {
                                StudentsGroupsListManager.archive(studentsGroup.name)
                                showToast(
                                        StringGetter(
                                                R.string.teacher_main_view_groups_options_archive_success,
                                                studentsGroup.name
                                        )
                                )
                            }

                        }
                    }
            ),

            Action(
                    title = StringGetter(R.string.teacher_main_view_groups_options_generate_register_code),
                    action = {
                        coroutinesExecutor.executeCoroutine {
                            val actionCode = StudentsGroupsListManager.generateRegistrationCode(studentsGroup.name)
                            ActionCodeType.CREATE_STUDENT_OF_GROUP.showInfoDialog(actionCode)
                        }
                    }
            )

    )

    private fun getArchivedStudentsGroupActions(
            studentsGroup: StudentsGroup,
            coroutinesExecutor: CoroutinesExecutor
    ) = listOf(

            Action(
                    title = StringGetter(R.string.teacher_main_view_groups_options_unarchive),
                    action = {
                        coroutinesExecutor.executeCoroutine {
                            StudentsGroupsListManager.unarchive(studentsGroup.name)
                            showToast(
                                    StringGetter(
                                            R.string.teacher_main_view_groups_options_unarchive_success,
                                            studentsGroup.name
                                    )
                            )
                        }
                    }
            ),

            Action(
                    title = StringGetter(R.string.teacher_main_view_groups_options_delete),
                    action = {
                        AppActivityConnector.showConfirmDialog(
                                title = StringGetter(R.string.teacher_main_view_groups_option_delete_confirm_dialog_title),
                                text = StringGetter(
                                        R.string.teacher_main_view_groups_option_delete_confirm_dialog_text,
                                        studentsGroup.name
                                ),
                                confirmText = StringGetter(R.string.teacher_main_view_groups_option_delete_confirm_dialog_button)
                        ) {
                            coroutinesExecutor.executeCoroutine {
                                StudentsGroupsListManager.delete(studentsGroup.name)
                                showToast(
                                        StringGetter(
                                                R.string.teacher_main_view_groups_options_delete_success,
                                                studentsGroup.name
                                        )
                                )
                            }

                        }
                    }
            )

    )

    fun getStudentsGroupActions(
            studentsGroup: StudentsGroup,
            coroutinesExecutor: CoroutinesExecutor
    ) =
            if (studentsGroup.archived) {
                getArchivedStudentsGroupActions(studentsGroup, coroutinesExecutor)
            } else {
                getUnarchivedStudentsGroupActions(studentsGroup, coroutinesExecutor)
            }

}