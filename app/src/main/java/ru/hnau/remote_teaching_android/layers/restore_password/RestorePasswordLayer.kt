package ru.hnau.remote_teaching_android.layers.restore_password

import android.content.Context
import android.text.InputFilter
import android.text.InputType
import android.text.method.PasswordTransformationMethod
import ru.hnau.androidutils.context_getters.StringGetter
import ru.hnau.androidutils.ui.view.addLinearSeparator
import ru.hnau.androidutils.ui.view.layer.layer.LayerState
import ru.hnau.androidutils.ui.view.utils.setPadding
import ru.hnau.remote_teaching_android.R
import ru.hnau.remote_teaching_android.data.AuthManager
import ru.hnau.remote_teaching_android.layers.base.AppLayer
import ru.hnau.remote_teaching_android.layers.main.MainLayer
import ru.hnau.remote_teaching_android.ui.hierarchy_utils.addBottomButtonView
import ru.hnau.remote_teaching_android.ui.hierarchy_utils.addFgSmallInputLabelView
import ru.hnau.remote_teaching_android.ui.hierarchy_utils.addFgSmallLabelView
import ru.hnau.remote_teaching_android.ui.input.InputViewInfo
import ru.hnau.remote_teaching_android.ui.input.addInput
import ru.hnau.remote_teaching_android.utils.managers.SizeManager
import ru.hnau.remote_teaching_common.utils.Validators
import ru.hnau.remote_teaching_common.data.UserRole
import ru.hnau.remote_teaching_common.exception.ApiException
import java.lang.IllegalArgumentException


class RestorePasswordLayer(
    context: Context
) : AppLayer(
    context = context
) {

    companion object {

        fun newInstance(
            context: Context,
            role: UserRole
        ) = RestorePasswordLayer(context).apply {
            this.restorePasswordContext = when (role) {
                UserRole.TEACHER -> RestorePasswordContext.Teacher
                UserRole.STUDENT -> RestorePasswordContext.Student
                else -> throw IllegalArgumentException("Unsupported for restore password role '${role.name}'")
            }
        }

    }

    override val title = StringGetter(R.string.restore_password_layer_title)

    @LayerState
    private lateinit var restorePasswordContext: RestorePasswordContext

    override fun afterCreate() {
        super.afterCreate()

        setPadding(SizeManager.LARGE_SEPARATION, SizeManager.DEFAULT_SEPARATION)

        addFgSmallInputLabelView(StringGetter(R.string.restore_password_layer_password_title))

        val passwordInput = addInput(
            info = InputViewInfo(
                maxLength = Validators.MAX_PASSWORD_LENGTH,
                transformationMethod = PasswordTransformationMethod.getInstance(),
                inputType = InputType.TYPE_TEXT_VARIATION_PASSWORD
            )
        )


        addFgSmallInputLabelView(StringGetter(R.string.restore_password_layer_password_repeat_title))

        val passwordRepeatInput = addInput(
            info = InputViewInfo(
                maxLength = Validators.MAX_PASSWORD_LENGTH,
                transformationMethod = PasswordTransformationMethod.getInstance(),
                inputType = InputType.TYPE_TEXT_VARIATION_PASSWORD
            )
        )

        addFgSmallInputLabelView(StringGetter(R.string.restore_password_layer_action_code_title))

        val actionCodeInput = addInput(
            info = InputViewInfo(
                maxLength = restorePasswordContext.actionCodeType.codeLength
            )
        ) {
            filters += InputFilter.AllCaps()
        }

        addFgSmallLabelView(restorePasswordContext.actionCodeComment)

        addLinearSeparator()

        addBottomButtonView(
            text = StringGetter(R.string.restore_password_layer_login),
            onClick = {
                restorePassword(
                    password = passwordInput.text.toString(),
                    passwordRepeat = passwordRepeatInput.text.toString(),
                    actionCode = actionCodeInput.text.toString()
                )
            }
        )

    }

    private fun restorePassword(
        password: String,
        passwordRepeat: String,
        actionCode: String
    ) {
        tryExecute {
            suspendLockedProducer.executeLocked {

                Validators.validateUserPasswordOrThrow(password)
                if (password != passwordRepeat) {
                    throw ApiException.raw(context.getString(R.string.restore_password_layer_passwords_are_different))
                }

                Validators.validateActionCodeOrThrow(actionCode, restorePasswordContext.actionCodeType)

                val login = restorePasswordContext.restorePassword(actionCode, password)

                AuthManager.login(login, password)
                showLayer(MainLayer(context), true)
            }
        }
    }

}