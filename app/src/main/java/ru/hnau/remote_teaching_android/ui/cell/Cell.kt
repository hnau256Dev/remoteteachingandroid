package ru.hnau.remote_teaching_android.ui.cell

import android.content.Context
import ru.hnau.androidutils.ui.view.clickable.ClickableLinearLayout
import ru.hnau.androidutils.ui.view.list.base.BaseListViewWrapper
import ru.hnau.remote_teaching_android.utils.managers.ColorManager


abstract class Cell<T : Any>(
    context: Context,
    private val onClick: (T) -> Unit
) : ClickableLinearLayout(
    context = context,
    rippleDrawInfo = ColorManager.PRIMARY_ON_TRANSPARENT_RIPPLE_INFO
), BaseListViewWrapper<T> {

    override val view = this

    protected var content: T? = null
        private set(value) {
            if (field != value && value != null) {
                field = value
                onContentReceived(value)
            }
        }

    override fun onClick() {
        super.onClick()
        content?.let(onClick)
    }

    override final fun setContent(content: T, position: Int) {
        this.content = content
    }

    protected abstract fun onContentReceived(content: T)

}