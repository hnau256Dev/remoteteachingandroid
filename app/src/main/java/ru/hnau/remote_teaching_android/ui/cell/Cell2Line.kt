package ru.hnau.remote_teaching_android.ui.cell

import android.content.Context
import ru.hnau.androidutils.context_getters.StringGetter
import ru.hnau.androidutils.ui.utils.h_gravity.HGravity
import ru.hnau.androidutils.ui.view.label.Label
import ru.hnau.androidutils.ui.view.utils.*
import ru.hnau.remote_teaching_android.utils.managers.ColorManager
import ru.hnau.remote_teaching_android.utils.managers.FontManager
import ru.hnau.remote_teaching_android.utils.managers.SizeManager


abstract class Cell2Line<T : Any>(
    context: Context,
    onClick: (T) -> Unit
) : Cell<T>(
    context,
    onClick
) {

    data class Data(
        val text: StringGetter,
        val subtitle: StringGetter,
        val active: Boolean = true
    )

    private val subtitleView = Label(
        context = context,
        fontType = FontManager.DEFAULT,
        gravity = HGravity.START_CENTER_VERTICAL,
        textSize = SizeManager.TEXT_16,
        minLines = 1,
        maxLines = 1,
        textColor = ColorManager.FG_T50
    )

    private val textView = Label(
        context = context,
        fontType = FontManager.DEFAULT,
        gravity = HGravity.START_CENTER_VERTICAL,
        textSize = SizeManager.TEXT_20,
        minLines = 1,
        maxLines = 1
    ).apply {
        setVerticalPadding(SizeManager.SMALL_SEPARATION)
    }

    init {
        orientation = VERTICAL
        setCenterForegroundGravity()
        setHorizontalPadding(SizeManager.DEFAULT_SEPARATION)
        setVerticalPadding(SizeManager.SMALL_SEPARATION)

        addView(textView)
        addView(subtitleView)
    }

    override final fun onContentReceived(content: T) {
        val (text, subtitle, active) = convertContentToData(content)
        subtitleView.text = subtitle
        textView.text = text
        textView.textColor = if (active) ColorManager.PRIMARY else ColorManager.FG_T50
    }

    protected abstract fun convertContentToData(content: T): Data

}