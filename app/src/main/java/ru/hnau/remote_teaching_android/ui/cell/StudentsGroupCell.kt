package ru.hnau.remote_teaching_android.ui.cell

import android.content.Context
import ru.hnau.androidutils.context_getters.StringGetter
import ru.hnau.androidutils.context_getters.toGetter
import ru.hnau.remote_teaching_android.R
import ru.hnau.remote_teaching_common.data.StudentsGroup


class StudentsGroupCell(
    context: Context,
    onClick: (StudentsGroup) -> Unit
) : Cell2Line<StudentsGroup>(
    context,
    onClick
) {

    override fun convertContentToData(content: StudentsGroup) =
        Data(
            text = content.name.toGetter(),
            subtitle = StringGetter(
                if (content.archived) {
                    R.string.students_group_status_archived
                } else {
                    R.string.students_group_status_unarchived
                }
            ),
            active = !content.archived
        )

}