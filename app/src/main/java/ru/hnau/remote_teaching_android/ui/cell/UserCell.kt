package ru.hnau.remote_teaching_android.ui.cell

import android.annotation.SuppressLint
import android.content.Context
import ru.hnau.androidutils.context_getters.toGetter
import ru.hnau.jutils.handle
import ru.hnau.remote_teaching_android.utils.extensions.fio
import ru.hnau.remote_teaching_android.utils.extensions.title
import ru.hnau.remote_teaching_common.data.User

@SuppressLint("ViewConstructor")
class UserCell(
    context: Context,
    onClick: (User) -> Unit
) : Cell2Line<User>(
    context,
    onClick
) {

    override fun convertContentToData(content: User) =
        content.fio.handle(
            ifNotNull = { fio ->
                Data(
                    text = fio.toGetter(),
                    subtitle = content.role.title + " (${content.login})"
                )
            },
            ifNull = {
                Data(
                    text = content.login.toGetter(),
                    subtitle = content.role.title
                )
            }
        )

}