package ru.hnau.remote_teaching_android.ui.empty_info

import ru.hnau.androidutils.context_getters.dp_px.DpPxGetter
import ru.hnau.androidutils.ui.utils.h_gravity.HGravity
import ru.hnau.androidutils.ui.view.label.LabelInfo
import ru.hnau.remote_teaching_android.ui.button.RTButtonInfo
import ru.hnau.remote_teaching_android.utils.managers.ColorManager
import ru.hnau.remote_teaching_android.utils.managers.FontManager
import ru.hnau.remote_teaching_android.utils.managers.SizeManager


data class EmptyInfoViewInfo(
    val title: LabelInfo = LabelInfo(
        fontType = FontManager.DEFAULT,
        textSize = SizeManager.TEXT_20,
        textColor = ColorManager.FG,
        gravity = HGravity.CENTER
    ),
    val buttonInfo: RTButtonInfo = RTButtonInfo.SMALL_PRIMARY_TEXT_AND_BORDER,
    val paddingHorizontal: DpPxGetter = SizeManager.LARGE_SEPARATION,
    val paddingVertical: DpPxGetter = SizeManager.LARGE_SEPARATION,
    val titleButtonSeparation: DpPxGetter = SizeManager.DEFAULT_SEPARATION
) {

    companion object {

        val DEFAULT = EmptyInfoViewInfo()

    }

}