package ru.hnau.remote_teaching_android.ui.hierarchy_utils

import android.view.ViewGroup
import ru.hnau.androidutils.context_getters.StringGetter
import ru.hnau.androidutils.ui.view.utils.MATCH_PARENT
import ru.hnau.androidutils.ui.view.utils.WRAP_CONTENT
import ru.hnau.androidutils.ui.view.utils.setLayoutParams
import ru.hnau.androidutils.ui.view.utils.setTopPadding
import ru.hnau.remote_teaching_android.ui.button.RTButton
import ru.hnau.remote_teaching_android.ui.button.RTButtonInfo
import ru.hnau.remote_teaching_android.ui.button.addRTButton
import ru.hnau.remote_teaching_android.utils.managers.SizeManager

fun ViewGroup.addButtonView(
    text: StringGetter,
    onClick: () -> Unit,
    info: RTButtonInfo,
    viewConfigurator: (RTButton.() -> Unit)? = null
) = addRTButton(text, onClick, info) {
    setTopPadding(SizeManager.DEFAULT_SEPARATION)
    setLayoutParams(WRAP_CONTENT, WRAP_CONTENT)
    viewConfigurator?.invoke(this)
}

fun ViewGroup.addLargePrimaryBackgroundShadowButtonView(
    text: StringGetter,
    onClick: () -> Unit,
    viewConfigurator: (RTButton.() -> Unit)? = null
) = addButtonView(text, onClick, RTButtonInfo.LARGE_PRIMARY_BACKGROUND_SHADOW, viewConfigurator)

fun ViewGroup.addSmallPrimaryTextAndBorderButtonView(
    text: StringGetter,
    onClick: () -> Unit,
    viewConfigurator: (RTButton.() -> Unit)? = null
) = addButtonView(text, onClick, RTButtonInfo.SMALL_PRIMARY_TEXT_AND_BORDER, viewConfigurator)

fun ViewGroup.addBottomButtonView(
    text: StringGetter,
    onClick: () -> Unit,
    viewConfigurator: (RTButton.() -> Unit)? = null
) = addButtonView(text, onClick, RTButtonInfo.LARGE_PRIMARY_BACKGROUND_SHADOW) {
    setLayoutParams(MATCH_PARENT, WRAP_CONTENT)
    viewConfigurator?.invoke(this)
}

fun ViewGroup.addSmallFgUnderlineTextButtonView(
    text: StringGetter,
    onClick: () -> Unit,
    viewConfigurator: (RTButton.() -> Unit)? = null
) = addButtonView(text, onClick, RTButtonInfo.SMALL_FG_TEXT_UNDERLINE, viewConfigurator)
