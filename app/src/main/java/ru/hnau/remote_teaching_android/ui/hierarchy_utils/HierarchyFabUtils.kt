package ru.hnau.remote_teaching_android.ui.hierarchy_utils

import android.widget.FrameLayout
import ru.hnau.androidutils.context_getters.DrawableGetter
import ru.hnau.androidutils.ui.view.buttons.circle.CircleButtonSize
import ru.hnau.androidutils.ui.view.buttons.circle.icon.CircleIconButton
import ru.hnau.androidutils.ui.view.buttons.circle.icon.addCircleIconButton
import ru.hnau.androidutils.ui.view.utils.WRAP_CONTENT
import ru.hnau.androidutils.ui.view.utils.setEndBottomGravity
import ru.hnau.androidutils.ui.view.utils.setFrameParams
import ru.hnau.androidutils.ui.view.utils.setMargins
import ru.hnau.remote_teaching_android.utils.managers.ColorManager
import ru.hnau.remote_teaching_android.utils.managers.SizeManager

fun FrameLayout.addPrimaryFab(
    icon: DrawableGetter,
    onClick: () -> Unit,
    viewConfigurator: (CircleIconButton.() -> Unit)? = null
) = addCircleIconButton(
    icon = icon,
    onClick = onClick,
    size = CircleButtonSize.DEFAULT,
    rippleDrawInfo = ColorManager.BG_ON_PRIMARY_RIPPLE_INFO,
    shadowInfo = ColorManager.DEFAULT_BUTTON_SHADOW_INFO
) {
    setFrameParams(WRAP_CONTENT, WRAP_CONTENT) {
        setMargins(SizeManager.DEFAULT_SEPARATION.getPxInt(context))
        setEndBottomGravity()
    }
    viewConfigurator?.invoke(this)
}