package ru.hnau.remote_teaching_android.ui.hierarchy_utils

import android.view.View
import android.view.ViewGroup
import ru.hnau.androidutils.ui.view.waiter.loader.SuspendLoader
import ru.hnau.androidutils.ui.view.waiter.loader.addSuspendLoader
import ru.hnau.jutils.coroutines.SuspendProducer
import ru.hnau.remote_teaching_android.ui.empty_info.EmptyInfoView
import ru.hnau.remote_teaching_android.utils.managers.ColorManager
import ru.hnau.remote_teaching_android.utils.managers.ErrorHandler

fun <T : Any> ViewGroup.addSuspendLoaderView(
        producer: SuspendProducer<T>,
        contentViewGenerator: (T) -> View,
        viewConfigurator: (SuspendLoader<T>.() -> Unit)? = null
) = addSuspendLoader(
        producer = producer,
        viewChangerInfo = ColorManager.DEFAULT_SUSPEND_CACHED_GETTER_LOADER_INFO,
        errorViewGenerator = {
            ErrorHandler.handle(it)
            EmptyInfoView.createLoadError(
                    context = context,
                    onButtonClick = producer::clear
            )
        },
        contentViewGenerator = contentViewGenerator,
        waiterViewGenerator = { ColorManager.createWaiterView(context, it) },
        viewConfigurator = viewConfigurator
)