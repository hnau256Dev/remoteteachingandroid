package ru.hnau.remote_teaching_android.ui.hierarchy_utils

import android.view.ViewGroup
import ru.hnau.androidutils.ui.view.waiter.WaiterView
import ru.hnau.jutils.producer.locked_producer.LockedProducer
import ru.hnau.remote_teaching_android.utils.managers.ColorManager

fun ViewGroup.addWaiter(
    lockedProducer: LockedProducer,
    viewConfigurator: (WaiterView.() -> Unit)? = null
) = addView(
    ColorManager.createWaiterView(context, lockedProducer).apply {
        viewConfigurator?.let(this::apply)
    }
)