package ru.hnau.remote_teaching_android.ui.input


import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Canvas
import android.text.InputFilter
import android.util.TypedValue
import android.view.ViewGroup
import android.widget.EditText
import ru.hnau.androidutils.context_getters.StringGetter
import ru.hnau.androidutils.ui.bounds_producer.ViewBoundsProducer
import ru.hnau.androidutils.ui.bounds_producer.addExtraSize
import ru.hnau.androidutils.ui.canvas_shape.RoundSidesRectCanvasShape
import ru.hnau.androidutils.ui.drawer.ExtraSize
import ru.hnau.androidutils.ui.drawer.border.BorderDrawer
import ru.hnau.androidutils.ui.view.getter.ViewGetter
import ru.hnau.androidutils.ui.view.getter.addView
import ru.hnau.androidutils.ui.view.utils.setPadding


@SuppressLint("ViewConstructor")
class InputView(
    context: Context,
    text: StringGetter = StringGetter.EMPTY,
    hint: StringGetter = StringGetter.EMPTY,
    info: InputViewInfo = InputViewInfo.DEFAULT
) : EditText(
    context
) {

    private val extraSize =
        info.border.extraSize.get(context) +
                ExtraSize(info.borderMargin.getPx(context))

    private val boundsProducer =
        ViewBoundsProducer(
            view = this,
            usePaddings = false
        )
            .addExtraSize(extraSize)

    private val canvasShape =
        RoundSidesRectCanvasShape(boundsProducer)

    private val borderDrawer = BorderDrawer(
        context = context,
        borderInfo = info.border,
        canvasShape = canvasShape
    )

    init {
        background = null
        setPadding(info.paddingHorizontal, info.paddingVertical)

        typeface = info.font.get(context).typeface
        setTextColor(info.textColor.get(context))
        setTextSize(TypedValue.COMPLEX_UNIT_PX, info.textSize.getPx(context))
        setText(text.get(context))

        info.maxLength?.let { filters += InputFilter.LengthFilter(it) }
        setRawInputType(info.inputType)
        setSingleLine()
        transformationMethod = info.transformationMethod

        setHintTextColor(info.hintTextColor.get(context))
        setHint(hint.get(context))
    }

    override fun draw(canvas: Canvas) {
        super.draw(canvas)
        borderDrawer.draw(canvas)
    }

}


fun input(
    text: StringGetter = StringGetter.EMPTY,
    hint: StringGetter = StringGetter.EMPTY,
    info: InputViewInfo = InputViewInfo.DEFAULT,
    viewConfigurator: (InputView.() -> Unit)? = null
) = ViewGetter(
    viewCreator = { InputView(it, text, hint, info) },
    viewConfigurator = viewConfigurator
)

fun ViewGroup.addInput(
    text: StringGetter = StringGetter.EMPTY,
    hint: StringGetter = StringGetter.EMPTY,
    info: InputViewInfo = InputViewInfo.DEFAULT,
    viewConfigurator: (InputView.() -> Unit)? = null
) =
    addView(
        InputView(context, text, hint, info),
        viewConfigurator
    )