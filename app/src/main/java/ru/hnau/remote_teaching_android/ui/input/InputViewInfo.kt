package ru.hnau.remote_teaching_android.ui.input

import android.text.InputType
import android.text.method.TransformationMethod
import ru.hnau.androidutils.context_getters.ColorGetter
import ru.hnau.androidutils.context_getters.dp_px.*
import ru.hnau.androidutils.ui.drawer.border.BorderInfo
import ru.hnau.androidutils.ui.font_type.FontTypeGetter
import ru.hnau.remote_teaching_android.utils.managers.ColorManager
import ru.hnau.remote_teaching_android.utils.managers.FontManager
import ru.hnau.remote_teaching_android.utils.managers.SizeManager


data class InputViewInfo(
    val textSize: DpPxGetter = SizeManager.TEXT_20,
    val textColor: ColorGetter = ColorManager.FG,
    val paddingHorizontal: DpPxGetter = SizeManager.LARGE_SEPARATION,
    val paddingVertical: DpPxGetter = SizeManager.DEFAULT_SEPARATION,
    val maxLength: Int? = null,
    val border: BorderInfo = BorderInfo(width = dp2, color = textColor, alpha = 1f),
    val borderMargin: DpPxGetter = dp8,
    val font: FontTypeGetter = FontManager.DEFAULT,
    val hintTextColor: ColorGetter = textColor.mapWithAlpha(0.5f),
    val inputType: Int = InputType.TYPE_CLASS_TEXT,
    val transformationMethod: TransformationMethod? = null
) {

    companion object {

        val DEFAULT = InputViewInfo()

    }

}