package ru.hnau.remote_teaching_android.ui.list

import android.annotation.SuppressLint
import android.content.Context
import ru.hnau.androidutils.ui.view.list.base.*
import ru.hnau.jutils.producer.Producer
import ru.hnau.remote_teaching_android.ui.cell.StudentsGroupCell
import ru.hnau.remote_teaching_android.utils.extensions.StudentsGroupCalculateDiffInfo
import ru.hnau.remote_teaching_common.data.StudentsGroup


@SuppressLint("ViewConstructor")
class StudentsGroupList(
    context: Context,
    itemsProducer: Producer<List<StudentsGroup>>,
    onClick: (StudentsGroup) -> Unit
) : BaseList<StudentsGroup>(
    context = context,
    itemsProducer = itemsProducer,
    viewWrappersCreator = { StudentsGroupCell(context, onClick) },
    orientation = BaseListOrientation.VERTICAL,
    fixedSize = true,
    calculateDiffInfo = StudentsGroupCalculateDiffInfo
)