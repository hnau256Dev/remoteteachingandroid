package ru.hnau.remote_teaching_android.ui.list

import android.annotation.SuppressLint
import android.content.Context
import ru.hnau.androidutils.ui.view.list.base.*
import ru.hnau.jutils.producer.Producer
import ru.hnau.remote_teaching_android.ui.cell.UserCell
import ru.hnau.remote_teaching_android.utils.extensions.UserCalculateDiffInfo
import ru.hnau.remote_teaching_common.data.User


@SuppressLint("ViewConstructor")
class UserList(
    context: Context,
    itemsProducer: Producer<List<User>>,
    onClick: (User) -> Unit
) : BaseList<User>(
    context = context,
    itemsProducer = itemsProducer,
    viewWrappersCreator = { UserCell(context, onClick) },
    orientation = BaseListOrientation.VERTICAL,
    fixedSize = true,
    calculateDiffInfo = UserCalculateDiffInfo
)