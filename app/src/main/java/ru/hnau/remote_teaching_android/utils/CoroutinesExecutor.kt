package ru.hnau.remote_teaching_android.utils

import kotlin.coroutines.CoroutineContext


interface CoroutinesExecutor {

    fun executeCoroutine(coroutine: suspend () -> Unit): Boolean

}