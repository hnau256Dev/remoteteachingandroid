package ru.hnau.remote_teaching_android.utils.extensions

import ru.hnau.remote_teaching_common.data.StudentsGroup


val StudentsGroup.sortKey: String
    get() = "${archived}_${name}"