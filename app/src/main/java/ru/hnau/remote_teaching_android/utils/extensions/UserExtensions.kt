package ru.hnau.remote_teaching_android.utils.extensions

import ru.hnau.androidutils.ui.view.list.base.BaseListCalculateDiffInfo
import ru.hnau.jutils.takeIfNotEmpty
import ru.hnau.remote_teaching_common.data.StudentsGroup
import ru.hnau.remote_teaching_common.data.User


val User.fio: String?
    get() = listOf(surname, name, patronymic)
        .mapNotNull { it.takeIfNotEmpty() }
        .joinToString(" ")
        .takeIfNotEmpty()

val User.fioOrLogin: String
    get() = fio ?: login

val User.sortKey: String
    get() = fioOrLogin

val UserCalculateDiffInfo = BaseListCalculateDiffInfo<User>(
    itemsComparator = { user1, user2 -> user1.login == user2.login },
    itemsContentComparator = { user1, user2 -> user1 == user2 }
)

val StudentsGroupCalculateDiffInfo = BaseListCalculateDiffInfo<StudentsGroup>(
    itemsComparator = { studentsGroup1, studentsGroup2 -> studentsGroup1.name == studentsGroup2.name },
    itemsContentComparator = { studentsGroup1, studentsGroup2 -> studentsGroup1 == studentsGroup2 }
)