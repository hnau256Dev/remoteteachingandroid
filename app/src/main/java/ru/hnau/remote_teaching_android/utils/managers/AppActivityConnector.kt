package ru.hnau.remote_teaching_android.utils.managers

import android.content.Context
import ru.hnau.androidutils.context_getters.StringGetter
import ru.hnau.androidutils.ui.view.layer.manager.LayerManagerConnector
import ru.hnau.androidutils.ui.view.layer.preset.dialog.view.bottom_sheet.BottomSheetView
import ru.hnau.androidutils.ui.view.layer.preset.dialog.view.material.MaterialDialogView
import ru.hnau.remote_teaching_android.AppActivity
import ru.hnau.remote_teaching_android.R
import ru.hnau.remote_teaching_android.layers.base.AppLayer
import ru.hnau.remote_teaching_android.ui.input.InputViewInfo


object AppActivityConnector {

    var appActivity: AppActivity? = null
        private set

    val layerManager: LayerManagerConnector?
        get() = appActivity?.layerManagerConnector

    fun onAppActivityCreated(appActivity: AppActivity) {
        AppActivityConnector.appActivity = appActivity
    }

    fun onAppActivityDestroyed(appActivity: AppActivity) = synchronized(this) {
        if (AppActivityConnector.appActivity == appActivity) {
            AppActivityConnector.appActivity = null
        }
    }

    fun showDialog(dialogViewConfigurator: MaterialDialogView.() -> Unit) {
        layerManager?.let {
            DialogManager.showDialog(
                it,
                dialogViewConfigurator
            )
        }
    }

    fun showConfirmDialog(
        title: StringGetter? = null,
        text: StringGetter? = null,
        confirmText: StringGetter = StringGetter(R.string.dialog_yes),
        onConfirm: () -> Unit
    ) {
        layerManager?.let {
            DialogManager.showConfirmDialog(
                it,
                title,
                text,
                confirmText,
                onConfirm
            )
        }
    }

    fun showInputDialog(
        title: StringGetter? = null,
        text: StringGetter? = null,
        inputInitialText: StringGetter = StringGetter.EMPTY,
        inputHint: StringGetter = StringGetter.EMPTY,
        inputInfo: InputViewInfo = InputViewInfo.DEFAULT,
        confirmButtonText: StringGetter = StringGetter(R.string.dialog_yes),
        onConfirm: (enteredText: String) -> Boolean
    ) {
        layerManager?.let {
            DialogManager.showInputDialog(
                it,
                title,
                text,
                inputInitialText,
                inputHint,
                inputInfo,
                confirmButtonText,
                onConfirm
            )
        }
    }

    fun showBottomSheet(
        bottomSheetViewConfigurator: BottomSheetView.() -> Unit
    ) {
        layerManager?.let {
            DialogManager.showBottomSheet(
                it,
                bottomSheetViewConfigurator
            )
        }
    }

    fun showLayer(
        layerBuilder: (Context) -> AppLayer,
        clearStack: Boolean = false
    ) {
        layerManager?.apply {
            showLayer(layerBuilder.invoke(viewContext), clearStack)
        }
    }

    fun goBack() =
        layerManager?.goBack() ?: false

}