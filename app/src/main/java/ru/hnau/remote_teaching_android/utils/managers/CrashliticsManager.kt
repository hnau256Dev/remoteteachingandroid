package ru.hnau.remote_teaching_android.utils.managers

import com.crashlytics.android.Crashlytics
import ru.hnau.remote_teaching_common.exception.ApiException


object CrashliticsManager {

    fun handle(message: String) =
        handle(ApiException.raw(message))

    fun handle(th: Throwable) {
        if (th is ApiException) {
            Crashlytics.log("Error: ${th.toString()}")
        } else {
            Crashlytics.logException(th)
        }
    }

}