package ru.hnau.remote_teaching_android.utils.managers

import ru.hnau.androidutils.context_getters.StringGetter
import ru.hnau.androidutils.ui.utils.Side
import ru.hnau.androidutils.ui.view.layer.manager.LayerManagerConnector
import ru.hnau.androidutils.ui.view.layer.preset.dialog.DialogLayer
import ru.hnau.androidutils.ui.view.layer.preset.dialog.view.bottom_sheet.BottomSheetView
import ru.hnau.androidutils.ui.view.layer.preset.dialog.view.bottom_sheet.BottomSheetViewInfo
import ru.hnau.androidutils.ui.view.layer.preset.dialog.view.bottom_sheet.item.BottomSheetItemInfo
import ru.hnau.androidutils.ui.view.layer.preset.dialog.view.bottom_sheet.text.BottomSheetTextInfo
import ru.hnau.androidutils.ui.view.layer.preset.dialog.view.bottom_sheet.title.BottomSheetTitleInfo
import ru.hnau.androidutils.ui.view.layer.preset.dialog.view.material.MaterialDialogView
import ru.hnau.androidutils.ui.view.layer.preset.dialog.view.material.MaterialDialogViewInfo
import ru.hnau.androidutils.ui.view.layer.preset.dialog.view.material.button.MaterialDialogButtonInfo
import ru.hnau.androidutils.ui.view.layer.preset.dialog.view.material.text.MaterialDialogTextInfo
import ru.hnau.androidutils.ui.view.layer.preset.dialog.view.material.title.MaterialDialogTitleInfo
import ru.hnau.androidutils.ui.view.layer.transaction.TransactionInfo
import ru.hnau.androidutils.ui.view.utils.postDelayed
import ru.hnau.jutils.TimeValue
import ru.hnau.remote_teaching_android.R
import ru.hnau.remote_teaching_android.ui.input.InputView
import ru.hnau.remote_teaching_android.ui.input.InputViewInfo


object DialogManager {

    fun showDialog(
        layerManager: LayerManagerConnector,
        dialogViewConfigurator: MaterialDialogView.() -> Unit
    ) {
        val dialogBuilder = MaterialDialogView.create(
            info = MATERIAL_DIALOG_VIEW_INFO,
            dialogViewConfigurator = dialogViewConfigurator
        )
        layerManager.showLayer(
            layer = DialogLayer.create(
                context = layerManager.viewContext,
                dialogViewBuilder = dialogBuilder
            ),
            transactionInfo = TransactionInfo(
                emersionSide = Side.BOTTOM
            )
        )
    }

    fun showConfirmDialog(
        layerManager: LayerManagerConnector,
        title: StringGetter? = null,
        text: StringGetter? = null,
        confirmButtonText: StringGetter = StringGetter(R.string.dialog_yes),
        onConfirm: () -> Unit
    ) = showDialog(layerManager) {
        title?.let(this::title)
        text?.let(this::text)
        cancelButton()
        closeButton(confirmButtonText, onClick = onConfirm)
    }

    fun showInputDialog(
        layerManager: LayerManagerConnector,
        title: StringGetter? = null,
        text: StringGetter? = null,
        inputInitialText: StringGetter = StringGetter.EMPTY,
        inputHint: StringGetter = StringGetter.EMPTY,
        inputInfo: InputViewInfo = InputViewInfo.DEFAULT,
        confirmButtonText: StringGetter = StringGetter(R.string.dialog_yes),
        onConfirm: (enteredText: String) -> Boolean
    ) = showDialog(layerManager) {

        val inputView = InputView(
            context = context,
            text = inputInitialText,
            info = inputInfo,
            hint = inputHint
        )

        title?.let(this::title)
        text?.let(this::text)
        view(inputView)
        cancelButton()

        button(confirmButtonText) {
            val enteredText = inputView.text.toString()
            val close = onConfirm.invoke(enteredText)
            if (close) {
                close()
            }
        }

        addOnClosedListener {
            KeyboardManager.hide()
        }

        postDelayed(TimeValue.MILLISECOND * 100) {
            KeyboardManager.showAndRequestFocus(inputView)
        }
    }

    fun showBottomSheet(
        layerManager: LayerManagerConnector,
        bottomSheetViewConfigurator: BottomSheetView.() -> Unit
    ) {
        val dialogBuilder = BottomSheetView.create(
            info = BOTTOM_SHEET_VIEW_INFO,
            dialogViewConfigurator = bottomSheetViewConfigurator
        )
        layerManager.showLayer(
            layer = DialogLayer.create(
                context = layerManager.viewContext,
                dialogViewBuilder = dialogBuilder
            ),
            transactionInfo = TransactionInfo(
                emersionSide = Side.BOTTOM
            )
        )
    }

    private val MATERIAL_DIALOG_VIEW_INFO = MaterialDialogViewInfo(
        button = MaterialDialogButtonInfo.DEFAULT.copy(
            textFont = FontManager.UBUNTU_BOLD,
            textColor = ColorManager.PRIMARY,
            rippleDrawInfo = ColorManager.PRIMARY_ON_TRANSPARENT_RIPPLE_INFO
        ),
        title = MaterialDialogTitleInfo.DEFAULT.copy(
            labelInfo = MaterialDialogTitleInfo.DEFAULT.labelInfo.copy(
                textColor = ColorManager.PRIMARY,
                fontType = FontManager.UBUNTU_BOLD
            )
        ),
        text = MaterialDialogTextInfo.DEFAULT.copy(
            labelInfo = MaterialDialogTextInfo.DEFAULT.labelInfo.copy(
                fontType = FontManager.UBUNTU
            )
        )
    )

    private val BOTTOM_SHEET_VIEW_INFO = BottomSheetViewInfo(
        title = BottomSheetTitleInfo.DEFAULT.copy(
            labelInfo = BottomSheetTitleInfo.DEFAULT.labelInfo.copy(
                textColor = ColorManager.PRIMARY,
                fontType = FontManager.UBUNTU_BOLD
            )
        ),
        text = BottomSheetTextInfo.DEFAULT.copy(
            labelInfo = BottomSheetTextInfo.DEFAULT.labelInfo.copy(
                fontType = FontManager.UBUNTU
            )
        ),
        item = BottomSheetItemInfo.DEFAULT.copy(
            labelInfo = BottomSheetItemInfo.DEFAULT.labelInfo.copy(
                textColor = ColorManager.PRIMARY,
                fontType = FontManager.UBUNTU
            ),
            rippleDrawInfo = ColorManager.PRIMARY_ON_TRANSPARENT_RIPPLE_INFO
        )
    )

}

fun MaterialDialogView.closeButton(onClick: () -> Unit = {}) =
    closeButton(StringGetter(R.string.dialog_close), onClick = onClick)

fun MaterialDialogView.cancelButton(onClick: () -> Unit = {}) =
    closeButton(StringGetter(R.string.dialog_cancel), onClick = onClick)

fun MaterialDialogView.yesButton(onClick: () -> Unit) =
    closeButton(StringGetter(R.string.dialog_yes), onClick = onClick)

fun MaterialDialogView.noButton(onClick: () -> Unit = {}) =
    closeButton(StringGetter(R.string.dialog_no), onClick = onClick)