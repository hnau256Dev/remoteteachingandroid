package ru.hnau.remote_teaching_android.utils.managers

import ru.hnau.androidutils.context_getters.dp_px.*
import ru.hnau.androidutils.context_getters.dp_px.DpPxGetter.Companion.dp


object SizeManager {

    val DEFAULT_SEPARATION = dp16
    val SMALL_SEPARATION = dp8
    val LARGE_SEPARATION = dp32

    val TEXT_24 = dp24
    val TEXT_20 = dp(20)
    val TEXT_16 = dp16
    val TEXT_12 = dp12

}