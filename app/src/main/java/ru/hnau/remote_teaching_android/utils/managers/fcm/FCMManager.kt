package ru.hnau.remote_teaching_android.utils.managers.fcm

import com.google.android.gms.tasks.Task
import com.google.firebase.iid.FirebaseInstanceId
import com.google.firebase.iid.InstanceIdResult
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock
import ru.hnau.androidutils.preferences.PreferencesManager
import ru.hnau.jutils.coroutines.launch
import ru.hnau.jutils.takeIfNotEmpty
import ru.hnau.remote_teaching_android.api.API
import ru.hnau.remote_teaching_android.data.AppInstanceManager
import ru.hnau.remote_teaching_android.utils.managers.CrashliticsManager


class FCMManager : FirebaseMessagingService() {

    companion object : PreferencesManager("FIREBASE_MESSAGES") {

        private var token by newStringProperty("TOKEN")

        fun sendPushTokenIfNeed() {
            FirebaseInstanceId.getInstance().instanceId.addOnCompleteListener() { task ->
                onTokenTaskCompleted(task)
            }
        }

        private fun onTokenTaskCompleted(task: Task<InstanceIdResult>) {
            if (!task.isSuccessful) {
                CrashliticsManager.handle(
                    Exception(
                        "Fcm pushToken receiving error",
                        task.exception
                    )
                )
                return
            }

            val token = task.result?.token?.takeIfNotEmpty()
            if (token == null) {
                CrashliticsManager.handle("Token is empty")
                return
            }

            onTokenChanged(token)
        }

        private val sendTokenMutex = Mutex()

        private fun onTokenChanged(token: String) {
            Dispatchers.IO.launch {
                sendTokenMutex.withLock {
                    if (FCMManager.token == token) {
                        return@launch
                    }
                    try {
                        API.updatePushToken(token, AppInstanceManager.uuid).await()
                        FCMManager.token = token
                    } catch (th: Throwable) {
                        CrashliticsManager.handle(th)
                    }
                }
            }
        }

    }

    override fun onNewToken(token: String?) {
        super.onNewToken(token)
        token ?: return
        onTokenChanged(token)
    }

    override fun onMessageReceived(message: RemoteMessage?) {
        super.onMessageReceived(message)
        FCMMessagesReceiver.onNewFcmMessage(message)
    }

}