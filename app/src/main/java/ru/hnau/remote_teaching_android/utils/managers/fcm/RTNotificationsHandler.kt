package ru.hnau.remote_teaching_android.utils.managers.fcm

import ru.hnau.androidutils.context_getters.toGetter
import ru.hnau.remote_teaching_android.utils.managers.CrashliticsManager
import ru.hnau.remote_teaching_android.utils.managers.notification.NotificationManager
import ru.hnau.remote_teaching_common.data.notification.RTNotification


object RTNotificationsHandler {

    fun onNewRTNotification(
            rtNotification: RTNotification
    ) = when (rtNotification) {

        is RTNotification.Common -> {
            NotificationManager.show(
                    text = rtNotification.message.toGetter()
            )
        }

        else -> {
            CrashliticsManager.handle("Unknown notification class ${rtNotification.javaClass.name}")
        }

    }

}