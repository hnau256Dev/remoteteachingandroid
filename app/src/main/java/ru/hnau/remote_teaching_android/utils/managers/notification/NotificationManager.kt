package ru.hnau.remote_teaching_android.utils.managers.notification

import android.app.Notification
import android.support.v4.app.NotificationCompat
import android.support.v4.app.NotificationManagerCompat
import ru.hnau.androidutils.context_getters.StringGetter
import ru.hnau.androidutils.utils.ContextConnector
import ru.hnau.remote_teaching_android.R
import ru.hnau.remote_teaching_android.utils.managers.notification.channels.NotificationChannelType
import ru.hnau.remote_teaching_android.utils.managers.notification.channels.NotificationChannelsManager


object NotificationManager {

    private const val DEFAULT_ID = 0

    private val notificationManager =
            NotificationManagerCompat.from(ContextConnector.context)

    fun show(
            text: StringGetter,
            id: Int = DEFAULT_ID,
            channelType: NotificationChannelType = NotificationChannelType.DEFAULT,
            title: StringGetter = StringGetter(R.string.app_name),
            iconResId: Int = R.drawable.ic_notification,
            //Tap handler (PendingIntent) setContentIntent
            configurator: NotificationCompat.Builder.() -> Unit = {}
    ) {
        val context = ContextConnector.context
        val notification = NotificationCompat.Builder(
                context, channelType.id
        ).apply {
            setContentTitle(title.get(context))
            setContentText(text.get(context))
            setSmallIcon(iconResId)
            configurator.invoke(this)
        }.build()
        show(id, notification)
    }

    private fun show(
            id: Int,
            notification: Notification
    ) {
        NotificationChannelsManager.initializeIfNeed()
        notificationManager.notify(id, notification)
    }

}